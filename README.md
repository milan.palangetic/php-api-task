# PHP API Task

Please create a simple REST API that would provide the user with the ability to manage simple Kanban boards. (Trello clone) This API must:

Follow REST web service standards regarding versioning, endpoint naming etc.
Be written in PHP. You can use any framework you want. Use of framework not required.
Have some basic documentation.

The application should allow user to do following:
*  Create/Edit/Delete kanban board (name|description)
*  Create/Edit/Delete lists inside board (title)
*  Create/Edit/Delete cards inside board lists (title|description)
*  Move cards between board lists

What would be good to have (bonus):
*  Authentication (oAuth2)  
*  Allow user to invite other users to it's boards
*  Allow user to assign cards to other users invited to the board

**NOTE:** It's not mandatory to finish all of the given tasks within the length of live coding interview, after time is up I'll review work done and evaluate based on that.

All code must be committed to a private Git repo on Bitbucket. Share the repository with user **milanpalangetic** in order for me to follow along. 
Commit history should show the progress being made during development. Please don't have only one commit with all your work.
Follow all PSR standards when formatting your code. Comment only when a method is not self explanatory. Use PHP 7.*.
Any tests will be greatly appreciated!
In the end, KISS is preferred, maintain SOLID principles and have fun with it!

Live coding will be conducted on Skype with screen sharing on.

Feel free to ask me for help/clarification when you're stuck, no judgment made. :)